package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_IN_ORDER = "売上ファイルが連番になっていません";
	private static final String NOT_EXIST = "が存在しません";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String INVALID_COAD_BR = "の支店コードが不正です";
	private static final String INVAID_CODE_COM = "の商品コードが不正です";
	private static final String EXCEEDED_DICITS = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数の確認 エラー処理（３－１）
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		String regexpBr = "^[0-9]{3}$";
		String regxepCom = "^[A-Za-z0-9]{8}$";

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店定義ファイル", regexpBr)) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品定義ファイル", regxepCom)) {
			return;
		}

		//処理内容（２－１、２－２）
		File[] files = new File(args[0]) .listFiles();
		List<File>rcdFiles = new ArrayList<>();

		//売上ファイルの名前とファイルであるかの判定をし該当ファイルを配列へ +エラー処理（３－３）
		for(int i = 0; i < files.length; i++) {
			String salesFilesName = files[i].getName();
			if(files[i].isFile() && salesFilesName.matches("^[0-9]{8}.*rcd$")){
				rcdFiles.add(files[i]);
			}
		}

		//ファイルをソート
		Collections.sort(rcdFiles);

		//売上ファイルの連番を確認 エラー処理（２－１）
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(files[i].getName().substring(0, 8));
			int latter = Integer.parseInt(files[i + 1].getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_IN_ORDER);
				return ;
			}
		}

		//2-2 集計
		//ファイル読み込み
		for(File saleFile: rcdFiles) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(saleFile);
				br = new BufferedReader(fr);

				String text;
				List<String> salesByBranch = new ArrayList<>();

				//Listに格納
				while((text = br.readLine()) != null) {
					salesByBranch.add(text);
				}
				String brCode = salesByBranch.get(0);
				String comCode = salesByBranch.get(1);
				String sales = salesByBranch.get(2);
				String fileName = saleFile.getName();

				//売上ファイルの中身フォーマット確認 エラー処理（２－５）    
				if(salesByBranch.size() != 3) {
					System.out.println(fileName + INVALID_FORMAT);
					return;
				}

				//売上ファイルの支店コード確認 エラー処理（２－３）
				if(!branchNames.containsKey(brCode)) {
					System.out.println(fileName + INVALID_COAD_BR);
					return ;
				}
				//売上ファイルの商品コード確認	エラー処理（２－４）
				if(!commodityNames.containsKey(comCode)) {
					System.out.println(fileName + INVAID_CODE_COM);
					return;
				}

				//ファイルから読み込んだ売上金額が数字でない場合 エラー処理（３－２）
				if(!sales.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//mapからvalueの取得と足し入れ
				long earnings = Long.parseLong(sales);
				Long totalSaleBr = branchSales.get(brCode) + earnings;
				Long totalSaleCom = commoditySales.get(comCode) + earnings;

				//売上金額桁数確認 エラー処理（２－２）	    
				if((totalSaleBr >= 10000000000L) || (totalSaleCom >= 10000000000L)){
					System.out.println(EXCEEDED_DICITS);
					return;
				}
				branchSales.put(brCode, totalSaleBr);
				commoditySales.put(comCode, totalSaleCom);
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				try {
					if(br != null) {
						// ファイルを閉じる
						br.close();
					}
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイルの書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		} 	
	}	

	/**
	 * 支店定義、商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名（商品コードと商品名）を保持するMap
	 * @param 支店コードと売上金額（商品コードと売上金額）を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long>sales, String processingFile, String regexp) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//支店定義ファイルが存在しない場合 エラー処理（１－１、１－３）
			if(!file.exists()) {
				System.out.println(processingFile + NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
				//処理（１－１、１－２、１－３、１－４）
				String[]items = line.split(",");

				//支店(商品)定義ファイルのフォーマットが不正な場合 エラー処理（１－２、１－４）
				if((items.length != 2) || (!items[0].matches(regexp))){
					System.out.println(processingFile + INVALID_FORMAT);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		//処理（３ー１、３－２）
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String code: names.keySet()) {
				String allsales = String.valueOf(sales.get(code));
				String name = names.get(code);
				bw.write(code + "," + name + "," + allsales);
				bw.newLine();
			}
			bw.close();
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
